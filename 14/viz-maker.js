const height = 600;
const width  = 600;

const url_geo_dpt = "https://france-geojson.gregoiredavid.fr/repo/departements.geojson";

let border_json;
let border_lineString;
let dpt_json;
let fleuves_json;
let cities_json;


let svg = d3.select("body").append("svg")
    .attr("height", height)
    .attr("width", width);

function remove_islands(features) {
    features.forEach(function(d, i){
	if(["2A", "2B"].includes(d.properties.code)){
	    features[i] = null;
	}
	else if(d.geometry.type === "MultiPolygon") {
	    let max_len = 0;
	    let max_len_j;
	    d.geometry.coordinates.forEach(function(c, j){
	    	if(c[0].length > max_len){
	    	    max_len = c[0].length;
	    	    max_len_j = j;
	    	}
	    });
	    d.geometry.coordinates = d.geometry.coordinates[max_len_j];
	    d.geometry.type = "Polygon";
	}
    });
}

/* -- Projection --*/
const l = height / 2;
const h = l * Math.sqrt(3) / 2;
const hexagone = {
    "O": [l                         ,     l    ],
    "A": [l - (l * Math.sqrt(3) / 2),     l / 2],
    "B": [l                         ,     0    ],
    "C": [l + (l * Math.sqrt(3) / 2),     l / 2],
    "D": [l + (l * Math.sqrt(3) / 2), 3 * l / 2],
    "E": [l                         , 2 * l    ],
    "F": [l - (l * Math.sqrt(3) / 2), 3 * l / 2]
};
let orthographic_projection = d3.geoOrthographic()
    .scale(3000)
    .rotate([-2.2, -46.8])
    .translate([width / 2, height / 2])
    .clipAngle(90)
    .precision(.1);
const geogone = {
    "O": orthographic_projection([ 2.2  , 46.8  ]), // Vers la Celle-Conde
    "A": orthographic_projection([-4.795, 48.409]), // Pointe de Corsen
    "B": orthographic_projection([ 2.545, 51.085]), // Frontiere cote Bray-Dunes
    "C": orthographic_projection([ 8.230, 48.963]), // Frontiere Lauterbourg
    "D": orthographic_projection([ 7.528, 43.782]), // Frontiere cote Garavan
    "E": orthographic_projection([ 3.174, 42.435]), // Frontiere cote Cerbere
    "F": orthographic_projection([-1.787, 43.377])  // Frontiere cote Hendaye
};

function get_angle(p){  // y in negative cause computers draw upside down
    return Math.atan2(- p[1] + geogone.O[1], p[0] - geogone.O[0]);
}
let geoangles = {};  // En radians
["A", "B", "C", "D", "E", "F"].forEach(function(point){
    geoangles[point] = get_angle(geogone[point]);
});
geoangles.FE = geoangles.E - geoangles.F;
geoangles.ED = geoangles.D - geoangles.E;
geoangles.DC = geoangles.C - geoangles.D;
geoangles.CB = geoangles.B - geoangles.C;
geoangles.BA = geoangles.A - geoangles.B;
geoangles.AF = geoangles.F - geoangles.A + 2 * Math.PI;


function get_hex_angle(x, y){
    let angle = get_angle([x, y]);
    let previous;
    let sixth_angle;
    let base_hex_angle;
    if (angle < geoangles.F) {
	previous = geoangles.A;
	sixth_angle = geoangles.AF;
	base_hex_angle = 5 * Math.PI / 6;
    }
    else if (angle < geoangles.E)  {
	previous = geoangles.F
	sixth_angle = geoangles.FE;
	base_hex_angle = - 5 * Math.PI / 6;
    }
    else if (angle < geoangles.D) {
	previous = geoangles.E;
	sixth_angle = geoangles.ED;
	base_hex_angle = - Math.PI / 2;
    }
    else if (angle < geoangles.C) {
	previous = geoangles.D;
	sixth_angle = geoangles.DC;
	base_hex_angle = - Math.PI / 6;
    }
    else if (angle < geoangles.B) {
	previous = geoangles.C;
	sixth_angle = geoangles.CB;
	base_hex_angle = Math.PI / 6;
    }
    else if (angle < geoangles.A) {
	previous = geoangles.B,
	sixth_angle = geoangles.BA;
	base_hex_angle = Math.PI / 2;
    }
    else {
	previous = geoangles.A;
	sixth_angle = geoangles.AF;
	base_hex_angle = 5 * Math.PI / 6;
    }

    let angle_to_previous = angle - previous;
    if (angle_to_previous < -Math.PI) {
	angle_to_previous = angle_to_previous + 2 * Math.PI;
    }
    let ratio_angle_to_previous = angle_to_previous / sixth_angle;
    let hex_angle = base_hex_angle + (ratio_angle_to_previous * Math.PI / 3);
    hex_angle = hex_angle > Math.PI ? hex_angle - 2 * Math.PI : hex_angle;
    return [hex_angle, ratio_angle_to_previous];
}

function get_hex_module(point, ratio_angle_to_previous) {
    let dist_to_center = turf.distance(point, [2.2, 46.8]);
    let dist_to_border = turf.pointToLineDistance(point, border_lineString);
    let dist_ratio = dist_to_center / (dist_to_center + dist_to_border);
    let angle_to_previous = ratio_angle_to_previous * Math.PI / 3;
    let angle_to_hauteur = Math.abs((Math.PI / 6) - angle_to_previous);
    let hex_dist_center_to_border = h / Math.cos(angle_to_hauteur);    
    return dist_ratio * hex_dist_center_to_border;
}

function hexprojette(point){
    [x, y] = orthographic_projection(point)
    let [hex_angle, ratio_angle_to_previous] = get_hex_angle(x, y);
    let hex_dist_to_center = get_hex_module(point, ratio_angle_to_previous);
    x =  geogone.O[0] + hex_dist_to_center * Math.cos(hex_angle);
    y =  geogone.O[1] - hex_dist_to_center * Math.sin(hex_angle);
    [x, y] = orthographic_projection.invert([x, y]);
    return [x, y];
}

function hexagonize_dpt(features){
    features.forEach(function(f){
	if(f !== null){
	    f.geometry.coordinates.forEach(function(c){
		c.forEach(function(point, i){
		    c[i] = hexprojette(point);
		});
	    });
	}
    });
}
function hexagonize_fleuves(features){
    features.forEach(function(f){
	f.geometry.coordinates.forEach(function(point, i){
	    f.geometry.coordinates[i] = hexprojette(point);
	});
    });
}
function hexagonize_cities(features){
    features.forEach(function(f){
	f.geometry.coordinates = hexprojette(f.geometry.coordinates);
    });
}


function main(){
    remove_islands(dpt_json.features);
    hexagonize_dpt(dpt_json.features);
    hexagonize_fleuves(fleuves_json.features);
    hexagonize_cities(cities_json.features);
    svg.selectAll("path.departement")
	.data(dpt_json.features)
	.enter().append("path")
	.attr("id", function(d){
	    if (d !== null) return "dpt-" + d.properties.nom;
	})
	.attr("class", "departement")
	.attr("d", d3.geoPath().projection(orthographic_projection))
	.attr("fill", "none")
	.attr("stroke", "#0002")
    ;
    svg.selectAll("path.fleuve")
	.data(fleuves_json.features)
	.enter().append("path")
	.attr("id", function(d){ return "fleuve-" + d.properties.nom; })
	.attr("class", "fleuve")
	.attr("d", d3.geoPath().projection(orthographic_projection))
	.attr("fill", "none")
	.attr("stroke", "blue")
	// .attr("stroke-width", 2)
    ;
    svg.selectAll("circle.ville")
	.data(cities_json.features)
	.enter().append("circle")
	.attr("id", function(d){ return "ville-" + d.properties.nom; })
	.attr("class", "ville")
	.attr("r", 2)
	.attr("cx", function(d){
	    return orthographic_projection(d.geometry.coordinates)[0];
	})
	.attr("cy", function(d){
	    return orthographic_projection(d.geometry.coordinates)[1];
	})
	.attr("fill", "black")
    ;
};


let border_loaded = d3.json("france.geo.json")
    .then(function(json){
	border_json = json;
	border_lineString = turf.polygonToLineString(border_json.features[0]);
    });
let dpt_loaded = d3.json(url_geo_dpt)
    .then(function(json){ dpt_json = json; });
let fleuves_loaded = d3.json("fleuves.json")
    .then(function(json){ fleuves_json = json; });
let cities_loaded = d3.json("cities.geo.json")
    .then(function(json){ cities_json = json; });

Promise.all([border_loaded, dpt_loaded, fleuves_loaded, cities_loaded])
    .then(main);
