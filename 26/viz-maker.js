const height = 600;
const width  = 600;

const base_params = {
    "scale": 300,
    "rotate": {"lon": 0, "lat": 0, "angle": 30},
};
let current_params = {
    "scale": 300,
    "rotate": {"lon": 0, "lat": 0, "angle": 30},
};

let body = d3.select("body");
let svg = body.append("svg")
    .attr("height", height)
    .attr("width", width);

let orthographic_projection = d3.geoOrthographic()
    .scale(base_params.scale)
    .rotate([base_params.rotate.lon, base_params.rotate.lat])
    .translate([width / 2, height / 2])
    .clipAngle(90)
    .precision(.1);

let pathes_areas = [];

d3.json("landmasses.geo.json").then(function(json){
    svg.selectAll("path")
	.data(json.geometries)
	.enter().append("path")
	.attr("class", function(d){
	    return "area-" + Math.floor(Math.log10(turf.area(d)));
	})
	.attr("d", d3.geoPath().projection(orthographic_projection))
	.attr("fill", "none")
    ;
    set_max_area(12);
});

function set_max_area(ordre){
    slider_print.text("Island surface < " + 10**(ordre-3) + " km²");
    for (let i = 1; i < 15; ++i) {
	d3.selectAll("path.area-" + i)
	    .attr("stroke", i > ordre ? "none" : "black");
    }
};

function update_display(){
    orthographic_projection
	.scale(current_params.scale)
	.rotate([current_params.rotate.lon, current_params.rotate.lat])
    ;
    svg.selectAll("path")
	.attr("d", d3.geoPath().projection(orthographic_projection));
}

/* zoom part */
body.append("button").text("↑").on("click", function(_){
    current_params.rotate.lat -= current_params.rotate.angle;
    if (current_params.rotate.lat < -90)
	current_params.rotate.lat = -90;
    update_display();
});
body.append("button").text("←").on("click", function(_){
    current_params.rotate.lon += current_params.rotate.angle;
    update_display();
});
body.append("button").text("→").on("click", function(_){
    current_params.rotate.lon -= current_params.rotate.angle;
    update_display();
});
body.append("button").text("↓").on("click", function(_){
    current_params.rotate.lat += current_params.rotate.angle;
    if (current_params.rotate.lat > 90)
	current_params.rotate.lat = 90;
    update_display();
});
body.append("button").text("+").on("click", function(_){
    current_params.scale *= 3/2;
    current_params.rotate.angle *= 2/3;
    update_display();
});
body.append("button").text("-").on("click", function(_){
    current_params.scale *= 2/3;
    current_params.rotate.angle *= 3/2;
    update_display();
});
body.append("button").text("🌍").on("click", function(_){
    current_params.scale = base_params.scale;
    current_params.rotate.lon = base_params.rotate.lon;
    current_params.rotate.lat = base_params.rotate.lat;
    current_params.rotate.angle = base_params.rotate.angle;
    update_display();
});



/* slider part */
let slider_div = d3.select("body").append("div") ;
let slider = slider_div.append("input")
    .attr("id", "slider")
    .attr("type", "range")
    .attr("value", "12")
    .attr("min", "2")
    .attr("max", "13")
    .attr("step", "1")
    .on("input", function(){ set_max_area(+this.value); })
;
let slider_print = slider_div.append("span").attr("id", "ordre");
