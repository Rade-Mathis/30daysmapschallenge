import json


def main():
    output_json = {
        "type":"GeometryCollection",
        "geometries": []
    }
    
    input_file = open("earth-coastlines-1km.geo.json", "r")
    input_json = json.load(input_file)
    input_file.close()

    for i in input_json["geometries"][0]["coordinates"]:
        x += len(i[0])
        output_json["geometries"].append({
            "type": "Polygon",
            "coordinates": i
        })

    output_file = open("landmasses.geo.json", "w")
    json.dump(output_json, output_file)
    output_file.close()
    
            
if __name__ == "__main__":
    main()
